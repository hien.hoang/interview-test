import os
import boto3
import uuid
import datetime as dt
import time


# Get Region config from environment vars or default to us-east-1
AWS_REGION = os.environ.get('AWS_REGION', 'us-east-1')

# Get Table Name from environment vars or default to test-users
DB_TABLE_NAME = os.environ.get('DB_TABLE_NAME', 'test-users')

DYNAMODB_CLIENT = boto3.resource('dynamodb', region_name=AWS_REGION)
DYNAMODB_TABLE = DYNAMODB_CLIENT.Table(DB_TABLE_NAME)

# Records time to live, default will auto delete after 2 weeks (14 days)
RECORDS_TTL = int(os.environ.get("RECORDS_TTL", 14 * 24 * 60 * 60))  


# ==================================================================================
# Main handler 
# ==================================================================================

def handler(event, context):
    """
    Handler - Main entry point
    ===
    For simplicity in this exercise, the event only contains first_name, last_name
    """

    # Since this event don't contains any sensity data, it's safe print the value here
    print(f"Creating user with info: {event}")

    body = {
        "uid": uuid.uuid4().hex, 
        "created_at": dt.datetime.today().isoformat(),
        "updated_at": dt.datetime.today().isoformat(),
        "ttl": int(time.time()) + RECORDS_TTL,
        **{k: event[k] for k in event if k in ["first_name", "last_name"]}
    }
    return save_data_to_db(body)


# ==================================================================================
# Data access layer 
# ==================================================================================

def save_data_to_db(data):
    '''
    Function saves data to DynamoDB table
    ===
    This helper function only take a simple job, save the data to DynamoDB
    '''
    result = DYNAMODB_TABLE.put_item(Item=data)
    return result
