import unittest
import boto3
import mock
import os
import uuid
from moto import mock_dynamodb2


DYNAMODB_TABLE_NAME = 'test-users'
DEFAULT_REGION = 'us-east-1'


@mock_dynamodb2
@mock.patch.dict(os.environ, {'DB_TABLE_NAME': DYNAMODB_TABLE_NAME})
class TestSaveDb(unittest.TestCase):

    def setUp(self):
        # Setup dynamo
        self.dynamodb = boto3.client('dynamodb')
        try:
            self.table = self.dynamodb.create_table(
                TableName=DYNAMODB_TABLE_NAME,
                KeySchema=[
                    {'KeyType': 'HASH', 'AttributeName': 'uid'}
                ],
                AttributeDefinitions=[
                    {'AttributeName': 'uid', 'AttributeType': 'S'}
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 5,
                    'WriteCapacityUnits': 5
                }
            )
        except self.dynamodb.exceptions.ResourceInUseException:
            print("Table existed")
        finally:
            self.table = boto3.resource('dynamodb').Table(DYNAMODB_TABLE_NAME)

    def test_save_data_to_db(self):
        from main import save_data_to_db
        save_data_to_db({"uid": uuid.uuid4().hex, "first_name": "Hien", "last_name": "Hoang"})

        db_response = self.table.scan(Limit=1)
        db_records = db_response['Items']
        self.assertEqual(len(db_records), 1)

