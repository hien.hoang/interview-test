import unittest
import boto3
import mock
import os
import uuid
from moto import mock_dynamodb2


DYNAMODB_TABLE_NAME = 'test-users'
DEFAULT_REGION = 'us-east-1'


@mock_dynamodb2
@mock.patch.dict(os.environ, {'DB_TABLE_NAME': DYNAMODB_TABLE_NAME})
class TestHandler(unittest.TestCase):

    def setUp(self):
        # Setup dynamo
        self.dynamodb = boto3.client('dynamodb')
        try:
            self.dynamodb.create_table(
                TableName=DYNAMODB_TABLE_NAME,
                KeySchema=[
                    {'KeyType': 'HASH', 'AttributeName': 'uid'}
                ],
                AttributeDefinitions=[
                    {'AttributeName': 'uid', 'AttributeType': 'S'}
                ],
                ProvisionedThroughput={
                    'ReadCapacityUnits': 5,
                    'WriteCapacityUnits': 5
                }
            )
        except self.dynamodb.exceptions.ResourceInUseException:
            print("Table exist")
        finally:
            self.table = boto3.resource('dynamodb').Table(DYNAMODB_TABLE_NAME)

    def test_handler(self):
        from main import handler
        handler({"first_name": "Hien", "last_name": "Hoang"}, {})

        db_response = self.table.scan(Limit=1)
        db_records = db_response['Items']
        self.assertEqual(len(db_records), 1)

