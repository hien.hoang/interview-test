
# Architecture

![](docs/architecture.png)

# Getting Started

## AWS SDK

- Install [aws cli](https://aws.amazon.com/cli/)

- Config the aws using command `aws configure`. [Lean more](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

## Install Requirements

- Python 3.8

- Install virtualenv

```shell
pip install virtualenv
```

- Create and activate virtual env

```shell
cd app/
virtualenv venv
source venv/bin/activate
```

- Install packages

```shell
pip install -r requirements.txt
pip install -r requirements-test.txt
```

# Testing

## Test Coverage

```shell
coverage run -m pytest
```

# Deployment

### 1. Bundle code

```shell
cd app/
zip lambda.zip main.py
```

### 2. Upload `lambda.zip` to a S3 bucket

- In my case, I deploy it to a bucket named `hien-interview-test`

### 3. Deploy with cloudformation

In the project root, run the command

```shell
aws cloudformation create-stack --stack-name hienapigw --template-body file://cloudformation.json --capabilities CAPABILITY_IAM --parameters ParameterKey=S3Bucket,ParameterValue=hien-interview-test
```

It should take about 30s to deploy the stack. Run this commmand to wait until it success

```shell
aws cloudformation wait stack-create-complete --stack-name hienapigw
```

### 4. Verify the deployment

```shell
aws cloudformation describe-stacks --stack-name hienapigw --query Stacks[0].Outputs
```

Output:

```yaml
- OutputKey: ApiId
  OutputValue: pgl3pxkh6f
```

Test the gateway using the url

```shell
API_ID=pgl3pxkh6f
ENDPOINT_URL="$API_ID.execute-api.us-east-1.amazonaws.com/v1"
curl -X POST -d '{"first_name": "Hien", "last_name": "Hoang"}' -H "Content-Type: application/json" https://$ENDPOINT_URL/users
```

# Explaining

#### Before being stored the event must be checked to ensure no numbers or special characters are in the string.

- Validation check using `JSON Schema` validation rules, define in cloudformation

```json
{
  "Type": "AWS::ApiGateway::Model",
  "Properties": {
    "Schema": {
      "properties": {
        "first_name": {
          "type": "string",
          "description": "Should contains only letters",
          "pattern": "^[a-zA-Z]+$"
        },
        "last_name": {
          "type": "string",
          "description": "Should contains only letters",
          "pattern": "^[a-zA-Z]+$"
        }
      }
    }
  }
}
```

- Enable the validator

```json
{
  "Type" : "AWS::ApiGateway::RequestValidator",
  "Properties" : {
    "ValidateRequestBody" : true,
  }
}
```

#### Enable CloudWatch

- AWS Lambda automatically monitors Lambda functions on your behalf, reporting metrics through Amazon CloudWatch. [Learn more](https://docs.aws.amazon.com/lambda/latest/dg/monitoring-cloudwatchlogs.html)

- 

#### Enable X-Ray

- Enable in API Gateway

```json
{
  "Type" : "AWS::ApiGateway::Stage",
  "Properties" : {
    "TracingEnabled": true
  }
},
```

- Enable in Lamda Function

```json
{
  "Type": "AWS::Lambda::Function",
  "Properties": {
    "TracingConfig": {
      "Mode": "Active"
    }
  }
}
```

#### Configure your storage so that records are automatically deleted after two weeks
- Enable auto delete expired records. [Learn more](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/time-to-live-ttl-how-to.html)
- Using cloudformation

```json
{
  "Type": "AWS::DynamoDB::Table",
  "Properties": {
    "TimeToLiveSpecification": {
      "AttributeName": "ttl",
      "Enabled": true
    }
  }
}
```

#### Consider the use of unit tests.

- All unittest inside `app/tests/`


#### Add a means to delay the event being stored by one minute.

It takes more time implement this idea, so I just explain the solution below.

I'm not sure I get the idea correctly, so my solution is to use a [Delay Queue](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-delay-queues.html) and set the delay time to 1 minute. All new requests will be sent to this queue, which deliver the message to another Lambda function to handle data storage. If the function is failed to process that message, it will be moved to a [dead-letter queues](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-dead-letter-queues.html) for retry or further investigation.

